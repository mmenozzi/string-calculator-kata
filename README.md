# String Calculator Kata #

This is a PHP implementation of the String Calculator TDD Kata:

http://osherove.com/tdd-kata-1/

## Project setup ##
    git clone git@bitbucket.org:fromagnoli/string-calculator-kata.git
    cd string-calculator-kata
    composer install

## Running tests ##
    vendor/bin/phpunit test/