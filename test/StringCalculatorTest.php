<?php
namespace Fabiana\StringCalculatorKata;


class StringCalculatorTest extends \PHPUnit_Framework_TestCase
{

    private $calculator;

    public function setUp()
    {
        $this->calculator = new StringCalculator();
    }

    public function testWithAtMostTwoNumbers()
    {
        $this->assertEquals(0, $this->calculator->add(""));
        $this->assertEquals(1, $this->calculator->add("1"));
        $this->assertEquals(3, $this->calculator->add("1,2"));
    }

    public function testWithMoreTwoNumbers()
    {
        $this->assertEquals(23, $this->calculator->add("10,5,3,5"));
        $this->assertEquals(55, $this->calculator->add("1,2,3,4,5,6,7,8,9,10"));
    }

    public function testWithNewlineAsSeparator()
    {
        $this->assertEquals(23, $this->calculator->add("10\n5\n3\n5"));
        $this->assertEquals(23, $this->calculator->add("10,5\n3,5"));
    }

    public function testWithCustomLineSeparator()
    {
        $this->assertEquals(23, $this->calculator->add("//;\n10;5;3;5"));
    }

    public function testWithNewlineAsCustomLineSeparator()
    {
        $this->assertEquals(23, $this->calculator->add("//\n\n10\n5\n3\n5"));
    }

    public function testWithCommaAsCustomLineSeparator()
    {
        $this->assertEquals(23, $this->calculator->add("//,\n10,5,3,5"));
    }

    public function testWithNegativeNumbers()
    {
        try {
            $this->calculator->add("1,2,-3");
            $this->fail('It should throw exception!');
        } catch (\Exception $e) {
            $this->assertEquals('negatives not allowed', $e->getMessage());
        }
    }

    public function testWithNumbersBiggerThan1000()
    {
        $this->assertEquals(23, $this->calculator->add("//,\n10,5,3,1001,5"));
    }

    public function testWithCustomDelimiterLongerThanOneCharacter()
    {
        $this->assertEquals(23, $this->calculator->add("//[***]\n10***5***3***5"));
    }

    public function testWithMultipleDelimiters()
    {
        $this->assertEquals(23, $this->calculator->add("//[;][|]\n10;5;3|5"));
    }

    public function testWithMultipleDelimitersLongerThanOne()
    {
        $this->assertEquals(23, $this->calculator->add("//[***][&&][|]\n10***5|3&&5"));
    }

    public function testDelimiterWithNumbers()
    {
        $this->assertEquals(23, $this->calculator->add("//[*7*][&&][|]\n10*7*5|3&&5"));
    }
}
